package com.carmina.gallery;

/**
 * Created by carmina on 31/01/2017.
 */

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;


public class ImageAdapter extends BaseAdapter {

    /** The context. */
    private Activity context;
    final ArrayList<String> images;

    /**
     * Instantiates a new image adapter.
     *
     */

    public ImageAdapter(Activity localContext) {

        context = localContext;
        images = getAllShownImagesPath(context);
    }


    @Override
    public boolean isEnabled (int position) {
        return true;
    }
    // returns the number of persons
    public int getCount() {
        return images.size();
    }

    // return the id of an item
    public String getItem(int position) {
        return images.get(position);
    }

    public long getItemId(int position) {
        return images.indexOf(getItem(position));
    }

    /**
     * Populate the gridView with Glide
     * Returns an ImageView
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public View getView(final int position, View convertView, ViewGroup parent) {

        ImageView picturesView;

        if (convertView == null) {
            picturesView = new ImageView(context);
            picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            picturesView.setLayoutParams(new GridView.LayoutParams(270, 270));

        } else {
            picturesView = (ImageView) convertView;
        }

        Glide.with(context).load(images.get(position))
                .placeholder(R.drawable.image_placeholder).centerCrop()
                .into(picturesView);

        return picturesView;
    }


    /**
     * The method for getting all images path from gallery, sd card and any other folders.
     *
     * @param activity the activity
     * @return ArrayList with images path
     */

    private ArrayList<String> getAllShownImagesPath(Activity activity) {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listOfImages = new ArrayList<String>();
        String absolutePathOfImage = null;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

        // returns a Cursor that is a pointer for the records in the results
        cursor = activity.getContentResolver().query(uri, projection, null, null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        //optional: if I will use folder view/galleries
        //column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);

        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            listOfImages.add(absolutePathOfImage);
        }
        return listOfImages;
    }

    public ArrayList<String> getImagesList() {
        return images;
    }


}


