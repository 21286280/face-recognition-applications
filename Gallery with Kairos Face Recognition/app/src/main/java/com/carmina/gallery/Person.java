package com.carmina.gallery;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by carmina on 31/01/2017.
 */

public class Person {

    /** The context. */
    private Activity context;
    private String name; // this is subjectId in Kairos methods
    private int image; // this is the enrolled image in Kairos methods
    private ArrayList<String> images;

    public Person (String name, ArrayList<String> images) {
        this.name = name;
        this.images = images;

    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

}
