package com.carmina.gallery;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.kairos.Kairos;
import com.kairos.KairosListener;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by carmina on 14/01/2017.
 */

public class KairosActivity extends Activity {

    /**
     * Called when the activity is first created.
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create an instance of the KairosListener
        KairosListener listener = new KairosListener() {
            @Override
            public void onSuccess(String response) {
                // code here
                Log.d("KAIROS DEMO", response);

            }

            @Override
            public void onFail(String response) {
                // code here
                Log.d("KAIROS DEMO", response);
            }
        };


        // instantiate a new Kairos instance
        Kairos myKairos = new Kairos();

        // set authentication
        String app_id = "991158ba";
        String api_key = "b87fa301e9bd2c5b5b2f10ed66f50330";
        myKairos.setAuthentication(this, app_id, api_key);


        try {

            // The LIST GALLERIES Method
            /**
             * This method lists out all galleries you have created.
             * The method returns a string response (list of all galleries)
             */

            //myKairos.listGalleries(listener);


            // The LIST ALL SUBJECTS IN A GALLERY Method
            /**
             * This method lists out all subjects enrolled in a given gallery.
             * The method returns a string response (list of all subjects in a given gallery).
             */

             String galleryId = "me";
             myKairos.listSubjectsForGallery(galleryId, listener);


            // The DETECT Method
            /**
             * The detect method returns you detailed information on face attributes for any images
             * you send it. It can provide you with coordinates of features such as eyes, nose, mouth.
             * The method returns a string response (face attribute data).
             */

            // Fine-grained Example with use of bitmap image and optional parameters
            Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.image_1);
            String selector = "FULL";
            //String minHeadScale = "0.25";

            myKairos.detect(image, selector, null, listener);

            // -----if returns "Complete" means that is a face in the picture


            // The ENROLL Method
            /**
             * The enroll methods let you register a face for later recognition.
             * You can enroll a new face, or enroll different photos of an already enrolled
             * for better recognition accuracy.
             * The method returns a string response (details of the enroll request).
             */

            // Fine-grained Example with use of bitmap image and optional parameters
            //Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.carmina);
            String subjectId = "Carmina";
            //String galleryId = "me";
            //String selector = "FULL";
            String multipleFaces = "false";
            String minHeadScale = "0.25";


            myKairos.enroll(image, subjectId, galleryId, selector, multipleFaces, minHeadScale, listener);
            // -----if returns "Complete" means that the person is registered with the given name=subjectId

            // The RECOGNISE Method
            /**
             * The recognize method attempts to recognize an image against a specified gallery,
             * and returns a list of subjectIds and their confidence values as possible matches.
             * For best recognition accuracy, it's recommended that you have multiple enrollments
             * (different images) per face.
             * The method returns a string response (list of possible matching subjects and their confidence values)
             */

            // Fine-grained example with use of bitmap image and optional parameters
            Bitmap image1 = BitmapFactory.decodeResource(getResources(), R.drawable.image_5); // success
            //Bitmap image2 = BitmapFactory.decodeResource(getResources(), R.drawable.paola); //failure
            //String galleryId = "me";
            //String selector = "FULL";
            String threshold = "0.65";
            //String minHeadScale = "0.25";
            String maxNumResults = "25";


            myKairos.recognize(image1, galleryId, selector, threshold, null, maxNumResults, listener);
                // -----if returns "Success" means that is the same person in the picture

            // The DELETE GALLERY Method
            /**
             * This method deletes a given gallery and all of its subjects.
             * The method returns a string response (confirmation of the results of the request)
             */

            //String galleryId = "me";
            //myKairos.deleteGallery(galleryId, listener);

            // The DELETE A SUBJECT FROM A GALLERY Method

            /**
             * This method deletes a given subject. All enrollments for the given subject will be
             * removed from the gallery.
             * The method returns a string response (confirmation of the results of the request).
             */

            //String subjectId = "Carmina";
            //String galleryId = "me";
            //myKairos.deleteSubject(subjectId, galleryId, listener);



        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

