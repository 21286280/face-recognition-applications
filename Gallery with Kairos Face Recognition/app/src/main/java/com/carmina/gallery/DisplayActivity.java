package com.carmina.gallery;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kairos.Kairos;
import com.kairos.KairosListener;

import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by carmina on 31/01/2017.
 */


public class DisplayActivity extends Activity {

    public String detectTime, enrolTime, recogniseTime;
    public String subjectId, subjectToDelete;

    public KairosListener listener;
    public Kairos myKairos;

    public Button btnDetect, btnEnroll, btnRecognise, btnDelete;
    public Bitmap bitmap,image;
    public ArrayList<String> list;
    public File file;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        final TextView kairosResponse = (TextView) findViewById(R.id.textKairos);
        final TextView detectTimeTxt = (TextView) findViewById(R.id.detectTime);
        final TextView enrolTimeTxt = (TextView) findViewById(R.id.enrolTime);
        final TextView recogniseTimeTxt = (TextView) findViewById(R.id.recogniseTime);

        btnDetect = (Button) findViewById(R.id.detect);
        btnEnroll = (Button) findViewById(R.id.enroll);
        btnRecognise = (Button) findViewById(R.id.recognise);
        btnDelete = (Button) findViewById(R.id.delete);

        // --------------------------------------------- KAIROS --------------------------------
        // Create an instance of the KairosListener
        listener = new KairosListener() {
            @Override
            public void onSuccess(String response) {
                // code here

                kairosResponse.setText(response);

                detectTimeTxt.setText("Detection Time: " + detectTime + " milliseconds");
                enrolTimeTxt.setText("Enrolment Time: " + enrolTime + " milliseconds");
                recogniseTimeTxt.setText("Recognition Time: " + recogniseTime + " milliseconds");


                saveToFile(kairosResponse.getText().toString());
                saveToFile(detectTime);
                saveToFile(enrolTime);
                saveToFile(recogniseTime);
                saveToFile(" ----------------------------------- ");

                Log.d("KAIROS DEMO", response);

            }

            @Override
            public void onFail(String response) {
                // code here
                Log.d("KAIROS DEMO", response);
            }
        };

        // instantiate a new Kairos instance
        myKairos = new Kairos();

        // set authentication
        String app_id = "991158ba";
        String api_key = "b87fa301e9bd2c5b5b2f10ed66f50330";
        myKairos.setAuthentication(this, app_id, api_key);

        // --------------------------------- DISPLAY ACTIVITY CODE -------------------------------

        // list containing all path pictures
        list = (new ImageAdapter(this)).getImagesList();

        // take the image possition from MainActivity
        Intent intent = getIntent();
        int position = intent.getExtras().getInt("imgPos");

        // get the absolute path for clicked picture and decode it
        String absolutePath = list.get(position);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        image = BitmapFactory.decodeFile(absolutePath, bmOptions);
        bitmap = getResizedBitmap(image, 1400);

        // upload the image in the ImageView
        ImageView imageView = (ImageView) findViewById(R.id.fullImage);
        imageView.setImageBitmap(bitmap);

        // DETECTION for clicked picture
        try {
            String selector = "FULL";

            long startTime = System.currentTimeMillis();

            //Method call
            // the bitmap is the clicked image
            myKairos.detect(bitmap, selector, null, listener);

            long endTime = System.currentTimeMillis() - startTime;
            detectTime = String.valueOf(endTime);

            Log.e("Detect Time:", detectTime);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Button to DETECT a person from the current gallery
        btnDetect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detectImage();

            }
        });

        // Button to ENROLL the picture
        btnEnroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enrollImage();

            }
        });

        // Button to DELETE a person from the current gallery
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteImage();

            }
        });

        // Button to RECOGNISE a person in the current gallery
        btnRecognise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recogniseImage();

            }
        });

    }

    private void detectImage() {

        try {
            String selector = "FULL";
            long startTime = System.currentTimeMillis();

            myKairos.detect(bitmap, selector, null, listener);

            long endTime = System.currentTimeMillis() - startTime;
            detectTime = String.valueOf(endTime);

            Log.e("Detect Time:", detectTime);

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void enrollImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(DisplayActivity.this);
        builder.setTitle("Enroll Person");

        LayoutInflater inflater = DisplayActivity.this.getLayoutInflater();
        // inflate and set the layout for the dialog
        // pass null as the parent view because its going in the dialog layout
        View dialog = inflater.inflate(R.layout.dialog_name, null);

        final EditText nameTxt = (EditText) dialog.findViewById(R.id.nameTxt);

        builder.setView(dialog)
                //Add action buttons for dialog
                .setPositiveButton(R.string.enroll, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //save the name from EditText and use it on enroll method

                        subjectId = nameTxt.getText().toString();
                        Log.e("Name of registration:", subjectId);

                        if (subjectId.matches("")) {
                            Toast.makeText(getApplicationContext(), "Please introduce a name", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                // Enrolment
                                String galleryId = "gallery";
                                String selector = "FULL";
                                String multipleFaces = "false";
                                String minHeadScale = "0.25";

                                long startTime = System.currentTimeMillis();
                                //Method call
                                myKairos.enroll(bitmap, subjectId, galleryId, selector, multipleFaces, minHeadScale, listener);
                                long endTime = System.currentTimeMillis() - startTime;
                                enrolTime = String.valueOf(endTime);

                                Log.e("Enrolment Time:", enrolTime);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }

                        dialogInterface.dismiss();
                    }
                })

                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        builder.show();

    }

    private void deleteImage() {

        AlertDialog.Builder builder = new AlertDialog.Builder(DisplayActivity.this);
        builder.setTitle("Delete person");

        LayoutInflater inflater = DisplayActivity.this.getLayoutInflater();

        // inflate and set the layout for the dialog
        // pass null as the parent view because its going in the dialog layout
        View dialogDelete = inflater.inflate(R.layout.dialog_delete, null);

        final EditText nameToDelete = (EditText) dialogDelete.findViewById(R.id.nameToDelete);

        builder.setView(dialogDelete)
                //Add action buttons for dialog
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        subjectToDelete = nameToDelete.getText().toString();
                        Log.e("Name to delete:", subjectToDelete);

                        if (subjectToDelete.matches("")) {
                            Toast.makeText(getApplicationContext(), "Please introduce a name:" + subjectToDelete, Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                String galleryId = "gallery";
                                myKairos.deleteSubject(subjectToDelete, galleryId, listener);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                        }
                        dialogInterface.dismiss();
                    }
                })

                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        builder.show();

    }

    private void recogniseImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(DisplayActivity.this);
        builder.setTitle("Recognise me again");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) // Take a Photo
            {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {

                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    Bitmap cameraPic = BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions);

                    //cameraPic = Bitmap.createScaledBitmap(bitmap, 70, 70, true);
                    //viewImage.setImageBitmap(cameraPic);

                    try {

                        // Recognition
                        String galleryId = "gallery";
                        String selector = "FULL";
                        String threshold = "0.50";
                        String maxNumResults = "25";

                        long startTime = System.currentTimeMillis();

                        //Method call
                        myKairos.recognize(cameraPic, galleryId, selector, threshold, null, maxNumResults, listener);

                        long endTime = System.currentTimeMillis() - startTime;
                        recogniseTime = String.valueOf(endTime);

                        Log.e("Recognise Time:", recogniseTime);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    String path = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {

                        outFile = new FileOutputStream(file);
                        cameraPic.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        outFile.flush();
                        outFile.close();

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2)  // Choose from Gallery
            {

                Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                Bitmap selectedPic = (BitmapFactory.decodeFile(picturePath));
                //Log.w("path of image", picturePath + "");

                try {

                    // Recognition
                    String galleryId = "gallery";
                    String selector = "FULL";
                    String threshold = "0.50";
                    //String minHeadScale = "0.25";
                    String maxNumResults = "25";

                    long startTime = System.currentTimeMillis();

                    //Method call
                    myKairos.recognize(selectedPic, galleryId, selector, threshold, null, maxNumResults, listener);

                    long endTime = System.currentTimeMillis() - startTime;
                    recogniseTime = String.valueOf(endTime);
                    Log.e("Recognise Time:", recogniseTime);

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //viewImage.setImageBitmap(selectedPic);
            }
        }
    }

    public static void saveToFile(String data) {
        final String fileName = "Kairos1400again.txt";
        final String path = Environment.getExternalStorageDirectory() + File.separator  + "kairos";
        try {
            new File(path).mkdir();
            File file = new File(path + fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file,true);
            fileOutputStream.write((data + System.getProperty("line.separator")).getBytes());

        }  catch(FileNotFoundException ex) {
            Log.d("TAG", ex.getMessage());
        }  catch(IOException ex) {
            Log.d("TAG", ex.getMessage());
        }

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}

